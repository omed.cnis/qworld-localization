# QWorld Localization


## Info

This repository manages the translations into different languages of the QWorld tutorials:

- [bronze-qiskit](https://gitlab.com/qworld/bronze-qiskit)
  - Arabic ([ar](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/ar))
  - Bengali ([bn](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/bn))
  - Chinese, Simplified ([zh_CN](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/zh_CN))
  - Chinese, Traditional ([zh_TW](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/zh_TW))
  - Czech ([cs](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/cs))
  - Dutch ([nl](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/nl))
  - Filipino ([fil](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fil))
  - French ([fr](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fr))
  - German ([de](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/de))
  - Italian ([it](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/it))
  - Kurdish ([ku](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/ku))
  - Nepali ([ne](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/ne))
  - Persian ([fa](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/fa))
  - Polish ([pl](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/pl))
  - Portuguese ([pt](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/pt))
  - Russian ([ru](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/ru))
  - Spanish ([es](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/es))
  - Tamil ([ta](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/ta))
  - Telugu ([te](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/te))
  - Turkish ([tr](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/tr))
  - Urdu ([ur](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/ur))
  - Vietnamese ([vi](https://gitlab.com/clausia/qworld-localization/-/tree/main/bronze-qiskit/vi))

- [nickel](https://gitlab.com/qworld/nickel)
  - Language (locale)<!--([es](https://gitlab.com/clausia/qworld-localization/-/tree/main/nickel/es))-->

- [silver](https://gitlab.com/qworld/silver)
  - Language (locale)
  

## Rules for the Womanium Project

All your work should be done in this repository.

Use the proper locale folder (listed above).

**Steps**
1. Create an issue to indicate that a particular file is going to be translated
   - example:
     - https://gitlab.com/clausia/qworld-localization/-/issues/4
2. When you have the translation, create a Merge Request for the modified file _(a Merge request (or MR) is the equivalent of a GitHub Pull Request (PR))_
   - The ideal way is to create a specific **branch** for the file that is being modified, from there the MR will be created
3. In an existing MR, indicate that this include the complete translation, ideally another person will come and indicate that they will proofread the file in said MR, but if you are the only one who is translating, then you will have to do the two steps yourself
   - you must leave a comment to indicate that you are working on the proofread
   - the comments that arise from the proofread are left in the same MR, if it is possible, indicate your comment in the relevant line of code
     - example of comments on a specific line, see MR: https://gitlab.com/clausia/qworld-localization/-/merge_requests/3 (just as an example)
4. The entire context is versioned, in each folder by language, that is, the folder structure, images, extra files, in this way you can: **fork** the repo (preferred method), clone the repo, or update the corresponding locale folder constantly in your local computer. The ideal is to have a local environment so that you can check the proper functioning of the notebook before doing the MR
5. It is not worth doing a half translation, that is, if you make an MR with an incomplete translation, it will be marked with a flag, if you want to start an MR with an incomplete translation, the MR must be in _Draft_, otherwise it will be marked
6. Once an MR has indicated that it has been proofread, then it can be _merged_
7. The work of a completed and _merged_ MR will be counted for both the translator and the proofreader (ideally, the proofreader should be someone other than the translator)

**Notes**
- The notebook file name must stay the same, it is not translated, and nothing is added to its name.
- Do not leave the outputs of the code cells if the original notebook does not have them, it is true that you have to validate the operation, but once that is done, you must eliminate the output (if applicable).
- The best thing to do is create a fork of this repo.
- Do not set aside too many notebooks by creating too many issues; set aside 1-3 and work on translations before creating more issues to set aside more notebooks.

### Criteria to consider for your Womanium project (considered individually)

- Translation quality
- Proofreading quality
- Number of notebooks translated
- Number of proofread notebooks
- Follow-up of the established rules
- Amount of typos found in your translations

<br>

--------

--------

**The following message was shared with all contributors on August 20, 2023 regarding their Womanium project:**

<br>


This is a reminder that the Womanium project ends on August 31, so here's a summary of the process for translations (note that this is a generic message and doesn't contain language-specific stuff)

1. Create one issue per notebook (a single file) to "set it aside" so that others know someone is already working on that translation.
2. Create a Merge Request (MR) with the translations of a single file
    - The MR must indicate in its description which issue will be closed when it is merged, to do so, use the word "closes" or "fixes" followed by the issue number:
> close #0
a complete list of the accepted words is here: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern
3. Proofread and indicate in the MR when finished; this should be done by someone other than the translator; we want to have good-quality translations
4. Once the process has been completed (steps 1 to 3), the MR can be merged
- Note that there must be an issue, an MR and the proofread for each translated file

- You can optionally add your name as a translator; under the line "prepared by," add a new line and then "translated by [Full Name]" (using the same italics format)

- Note that the translation of a single file cannot be considered as your Womanium project
- The criteria for it to count as your Womanium project are:
   - Translation quality
   - Proofreading quality
   - Number of notebooks translated
   - Number of proofread notebooks
   - Follow-up of the established rules
   - Amount of typos found in your translations
- Criteria are considered individually

- The last day to create an MR or proofread will be August 31 at 21:00 UTC; anything done after that date and time will not count towards your Womanium project
- We highly appreciate any translations that take place after the date

- Merged translated notebooks will count more than non-merged ones. The reason for a merge not taking place is the lack of any of the rules (mentioned at the beginning of this conversation and summarized in this same message).
